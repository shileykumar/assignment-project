package com.apica.controller;

import com.apica.model.dataObject.UserDO;
import com.apica.service.JournalService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping
@RequiredArgsConstructor
@CrossOrigin
public class JournalController {

    private final JournalService journalService;

    @GetMapping("/journal")
    public List<UserDO> getUserEvents() {
        return journalService.getUserEvents();
    }
}
