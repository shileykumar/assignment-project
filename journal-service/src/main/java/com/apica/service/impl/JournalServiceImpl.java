package com.apica.service.impl;

import com.apica.model.dataObject.UserDO;
import com.apica.repository.UserRepository;
import com.apica.service.JournalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Consumer;

@Service
@RequiredArgsConstructor
@Slf4j
public class JournalServiceImpl implements JournalService {

    private final StreamBridge streamBridge;

    private final UserRepository userRepository;

    @Override
    public List<UserDO> getUserEvents() {

        return userRepository.findAll();
    }

    @Bean
    public Consumer<Message<UserDO>> consumerUserBinding() {
        return message -> {
            log.info("Received User Event: {}", message.getPayload());
            UserDO userDO = message.getPayload();
            log.info("user :: {}", userDO);
            userRepository.save(userDO);
        };
    }
}
