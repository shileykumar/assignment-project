package com.apica.service;

import com.apica.model.dataObject.UserDO;

import java.util.List;

public interface JournalService {

    List<UserDO> getUserEvents();
}
