package com.apica.service;

import com.apica.model.dataObject.UserDO;
import com.apica.model.dto.CreateUserDTO;

import java.util.List;

public interface UserService {

    UserDO createUser(CreateUserDTO payload);

    List<UserDO> getUsers();

    UserDO findById(Integer id);

    void deleteUser(Integer id);
}
