package com.apica.service.impl;

import com.apica.model.dataObject.UserDO;
import com.apica.model.dto.CreateUserDTO;
import com.apica.repository.UserRepository;
import com.apica.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Supplier;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final StreamBridge streamBridge;

    @Override
    public UserDO createUser(CreateUserDTO payload) {
        log.info("Request to create User ::");
        UserDO user = UserDO.builder().firstName(payload.getFirstName()).lastName(payload.getLastName())
                .email(payload.getEmail()).mobile(payload.getMobile()).build();
        user = userRepository.save(user);
        streamBridge.send("produceUserBinding-out-0", user);
        return user;
    }

    @Override
    public List<UserDO> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public UserDO findById(Integer id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
    }
}
