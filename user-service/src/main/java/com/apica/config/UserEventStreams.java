package com.apica.config;

import org.springframework.messaging.MessageChannel;

public interface UserEventStreams {

    MessageChannel outboundUserEvents();
}
