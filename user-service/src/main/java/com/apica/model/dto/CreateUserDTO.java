package com.apica.model.dto;

import lombok.Data;

@Data
public class CreateUserDTO {

    private String firstName;

    private String lastName;

    private String mobile;

    private String email;
}
