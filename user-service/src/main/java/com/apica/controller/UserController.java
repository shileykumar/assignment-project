package com.apica.controller;

import com.apica.model.dataObject.UserDO;
import com.apica.model.dto.CreateUserDTO;
import com.apica.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping
@RequiredArgsConstructor
@Slf4j
@CrossOrigin
public class UserController {

    private final UserService userService;

    @PostMapping("/user")
    public ResponseEntity<UserDO> createUser(@RequestBody CreateUserDTO payload) throws URISyntaxException {
        log.info("Rest request to create user");
        UserDO result = userService.createUser(payload);

        return ResponseEntity
                .created(new URI("/api/user" + result.getId()))
                .body(result);
    }
}
